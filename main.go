package main

import (
	"log"
	"net/http"
	"os"
	"restapplication/cmd"
	"time"
)


var StartTime = time.Now();

func main(){
	cmd.StartTime = StartTime
	cmd.LastUsedId = -1

	port := os.Getenv("PORT")
	if port == ""{
		port = "8080"
	}

	http.HandleFunc("/repocheck/v1/commits/", cmd.CommitHandler)
	http.HandleFunc("/repocheck/v1/languages/", cmd.LanguageHandler)
	http.HandleFunc("/repocheck/v1/status/", cmd.StatusHandler)
	http.HandleFunc("/repocheck/v1/webhooks/", cmd.WebhooksHandler)
	http.HandleFunc("/repocheck/v1/webhooks", cmd.WebhooksHandler)

	log.Fatal(http.ListenAndServe(":" + port, nil))

}