package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)


var StartTime time.Time

var LastUsedId int
var allWebhooks []Webhooks

//url Which gives you 100 projects per page for the specified page number
var pageURL = "https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&page="

//https://git.gvk.idi.ntnu.no/api/v4/projects/ + ID
var ProjectWithID = "https://git.gvk.idi.ntnu.no/api/v4/projects/"

//URL with language for the project ID
var ProjectLanguageURL = "https://git.gvk.idi.ntnu.no/api/v4/projects/"

func CommitHandler(w http.ResponseWriter, r*http.Request){

	//tells the content type to be type json
	http.Header.Add(w.Header(), "Content-type", "application/json")

	URLParts := strings.Split(r.URL.Path, "/")
	numberOfRepos, _ := strconv.Atoi(URLParts[4])
	if URLParts[4] == ""{
		numberOfRepos = 5
	}

	var repository = &[]Repository{} //Array with Repository structs to save name and commits


	for i := 1; i <= 2; i++ {						// Loops through each URL page of repos
		var temp = &[]Repository{}
		response, err := http.Get(pageURL + strconv.Itoa(i)) //retrieves info from specified page number
		if err != nil{
			fmt.Printf("The HTTP request failed with error %s\n", err)
		}
		_ = json.NewDecoder(response.Body).Decode(temp)			// Fills the repo array with ID and names
		*repository = append(*repository, *temp...)	// Appends into array for each page
	}

	//loop through all elements in Repository
	for i, elem := range *repository{
		var NumOfCommits = &[]NumCommits{} //array to save IDs

		//retrieves ID of each commit in the specific repository
		response, err := http.Get(ProjectWithID + strconv.Itoa(elem.RepoID) + "/repository/commits")
		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
		}

		_ = json.NewDecoder(response.Body).Decode(NumOfCommits) //reads all commit IDs from the project into the array.
		(*repository)[i].Commits = len(*NumOfCommits)           //saves the number of commits into the specific Repository.
	}


	QuickSort(*repository) //sorts the Array sent in parameter

	highestCommits := make([]Repository, numberOfRepos) //array of type Repository with length = numberOfRepos

	//saves a number of Repositories equal to numberOfRepos into highestCommits
	if numberOfRepos <= len(*repository) {
		for i := 0; i < numberOfRepos; i++ {
			highestCommits[i] = (*repository)[i]
		}
		//prints to website.
		err := json.NewEncoder(w).Encode(highestCommits)
		if err != nil {
			panic(err)
		}
	}else {
		http.Error(w, "Number of languages must be equal or less than: " + strconv.Itoa(len(*repository)), http.StatusBadRequest)
	}
	//Invokes all the webhooks to this event
	sendToWebhooks(w, numberOfRepos, "COMMITS")
}


func LanguageHandler(w http.ResponseWriter, r*http.Request) {
	//tells the content type to be type json
	http.Header.Add(w.Header(), "Content-type", "application/json")

	//map for storing the language and percentage
	var ProgLanguages map[string]float32

	KeysCollection := make([]string, 0, len(ProgLanguages))

	URLParts := strings.Split(r.URL.Path, "/")
	numberOfLanguages, _ := strconv.Atoi(URLParts[4])
	if URLParts[4] == ""{
		numberOfLanguages = 5
	}

	var repo = &[]Repository{} //var for saving the ID

	for i := 1; i <= 2; i++ { // Loops through each URL page of repos
		var temp = &[]Repository{}
		response, err := http.Get(pageURL + strconv.Itoa(i)) //retrieves info from specified page number
		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
		}
		_ = json.NewDecoder(response.Body).Decode(temp) // Fills the repo array with ID and names
		*repo = append(*repo, *temp...)                 // Appends into array for each page
	}

	//loop for retrieving languages for every project
	for _, elem := range *repo {
		response, err := http.Get(ProjectLanguageURL + strconv.Itoa(elem.RepoID) + "/languages")
		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
		}

		//reads into ProgLanguages
		err = json.NewDecoder(response.Body).Decode(&ProgLanguages)
		if err != nil {
			panic(err)
		}

		//loop for appending language for each repo into array
		for k := range ProgLanguages {
			KeysCollection = append(KeysCollection, k)
		}
	}

	//Counts the number of occurences from the KeysCollection array
	counter := make(map[string]int)
	for _, row := range KeysCollection {
		counter[row]++
	}

	var SortStruct= []Language{} //var for saving the language key and frequency value
	for k, v := range counter {
		SortStruct = append(SortStruct, Language{k, v})
	}

	//Sorts the array of structs based on the frequency Value
	sort.SliceStable(SortStruct, func(i, j int) bool {
		return SortStruct[i].Value > SortStruct[j].Value
	})

	MostUsedLanguages := make([]Language, numberOfLanguages)

	//saves amount of languages equal to numberOfLanguages into MostUsedLanguages
	if numberOfLanguages <= len(SortStruct) {
		for i := 0; i < numberOfLanguages; i++ {
			MostUsedLanguages[i] = (SortStruct)[i]
		}
		err := json.NewEncoder(w).Encode(&MostUsedLanguages)
		if err != nil {
			panic(err)
		}
	}else {
		http.Error(w, "Number of languages must be equal or less than: " + strconv.Itoa(len(SortStruct)), http.StatusBadRequest)
	}
	//Invokes all the webhooks to this event
	sendToWebhooks(w, numberOfLanguages, "LANGUAGES")
}

func StatusHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet{
		http.Header.Add(writer.Header(), "content-type", "application/json")

		if r.URL.Path == "/repocheck/v1/status/"{
			URLParts := strings.Split(r.URL.Path, "/")
			var status = &Status{}

			testResponse, getError := http.Get(ProjectWithID)		//GET request to test status
			CheckError(getError, writer)						//Check for errors

			status.GitLabStatus = testResponse.StatusCode		//Sets the error code, should be 200

			closeErr := testResponse.Body.Close()				//Closes the reponse
			CheckError(closeErr, writer)						//Check for errors

			status.UpTime = int(Uptime()/1000000000)			//Gets uptime in seconds
			status.Version = URLParts[2]						//Sets version from client URL

			encodeErr := json.NewEncoder(writer).Encode(status)		//Encodes the struct to the client
			CheckError(encodeErr, writer)							//Check for errors

		} else { http.Error(writer, "Malformed URL", http.StatusBadRequest) }
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}

func WebhooksHandler(w http.ResponseWriter, reader*http.Request) {
	http.Header.Add(w.Header(), "content-type", "application/json")

	switch reader.Method {

	//Reads information about a webhook for user, andre stores it in an array
	case http.MethodPost:
		if reader.URL.Path == "/repocheck/v1/webhooks" || reader.URL.Path == "/repocheck/v1/webhooks/"{
			newWebhook := &Webhooks{}
			decodeError := json.NewDecoder(reader.Body).Decode(newWebhook) //Decodes information from user, to a struct
			CheckError(decodeError, w)                                     //Checks for errors
			LastUsedId++                                                   //Increases the ID for this webhook to use
			newWebhook.Id = LastUsedId                                     //Sets an ID for the webhook
			newWebhook.TimeStamp = time.Now()                              //Sets a timestamp for when the webhook was added

			allWebhooks = append(allWebhooks, *newWebhook)                  //Appens this struct to the array of all webhooks
			bytesWritten, printError := fmt.Fprintln(w, len(allWebhooks)-1) //Returns the ID of the wehook to the user

			CheckError(printError, w) //Checks for errors
			if bytesWritten == 0 {    //Checks if no bytes was written, then somwthing went wrong
				fmt.Println(fmt.Errorf("No text was written to page\n"))
			}

		} else { http.Error(w, "Malformed URL", http.StatusBadRequest) }


	//GET request to print out webhooks
	case http.MethodGet:
		URLParts := strings.Split(reader.URL.Path, "/")
		if reader.URL.Path == "/repocheck/v1/webhooks/" {	//If user wants all webhooks
			encodeError := json.NewEncoder(w).Encode(allWebhooks) //Encode all webhooks
			CheckError(encodeError, w)                            //And check for error
			//If not, but user want one specific webhook
		} else if len(URLParts) == 5 && URLParts[1] == "repocheck" && URLParts[2] == "v1" && URLParts[3] == "webhooks"{

			webhookId, convertError := strconv.Atoi(URLParts[4]) //Get this ID to a string
			CheckError(convertError, w)                          //Check for errors

			if webhookId <= LastUsedId {												//If asked id exists
				for i, object := range allWebhooks { 									//Go through all webhooks
					if object.Id == webhookId { 										//When you find the webhook
						encodeError := json.NewEncoder(w).Encode(allWebhooks[i]) //Encode this to user
						CheckError(encodeError, w)                               //And check for errors
					}
				}		//If asked id does not exist, tell the user so
			} else {http.Error(w, "Webhook does noe exist", http.StatusBadRequest)}
		}

	//Deletes the webhook with the id sendt from user
	case http.MethodDelete:
		if reader.URL.Path == "/repocheck/v1/webhooks" || reader.URL.Path == "/repocheck/v1/webhooks/"{

			bytesRead, readError := ioutil.ReadAll(reader.Body) //Reads the body of the result as a byte
			CheckError(readError, w)                            //Checks for errors

			webhookId, convertError := strconv.Atoi(string(bytesRead)) //Converts from bytes to string, then to int
			CheckError(convertError, w)                                //Checks for errors

			if webhookId <= LastUsedId {									//If asked id to delete exists
				box := 0
				tempArray := make([]Webhooks, len(allWebhooks)-1)			//Create a temp array
				for i, object := range allWebhooks {						//Go through all webhooks
					if object.Id != webhookId {								//If the webhook is noe the one we want to delete
						tempArray[box] = allWebhooks[i]						//Then copy this to the first available box in temparray
						box++												//Then set the next avalable box for nest turn
					}
				}

				//When everyone are copyed (exept the one to delete), the global array is set to become the temparray
				allWebhooks = tempArray

				//Tells the user if the webhook was deletens
				bytesWritten, printError := fmt.Fprintln(w, "Webhook with ID =", webhookId, "was deleted")

				CheckError(printError, w) //Checks for errors
				if bytesWritten == 0 {    //If nothing was written to user, then something went wrong
					fmt.Println(fmt.Errorf("No text was written to page\n"))
				}
				//Tells the user if the webhook does noe exist
			} else {http.Error(w, "Webhook does noe exist", http.StatusBadRequest)}
		}


	default:		//Tells the user that this action is noe available
		http.Error(w, "Invalid method "+reader.Method, http.StatusBadRequest)
	}
}


func sendToWebhooks(w http.ResponseWriter, params int, event string){
	for _, object := range allWebhooks{										//For alle webhooks
		if strings.ToUpper(object.Event) == event{							//If that webhook has the event from parameter
			var invocation = Invocation{Event: object.Event, Params: params, TimeStamp: time.Now()}		//Creates the struct to send

			data, marshalError := json.Marshal(invocation) //Marshals this to json format
			CheckError(marshalError, w)                    //Checks for errors

			//Sends a porst request with URL, Contenttype and the struct as a buffer
			_, postError := http.Post(object.Url, "application/json", bytes.NewBuffer(data))
			CheckError(postError, w) //Checks for errors
		}
	}
}



/**
* Function used from internet
* Source: https://www.varunpant.com/posts/basic-sorting-algorithms-implemented-in-golang
 */
func QuickSort(items []Repository) {
	if len(items) > 1 {
		pivotIndex := len(items) / 2
		var smallerItems []Repository
		var largerItems []Repository

		for i := range items {
			repo := items[i]
			if i != pivotIndex {
				if repo.Commits > items[pivotIndex].Commits {
					smallerItems = append(smallerItems, repo)
				} else {
					largerItems = append(largerItems, repo)
				}
			}
		}

		QuickSort(smallerItems)
		QuickSort(largerItems)

		var merged = append(append(append([]Repository{}, smallerItems...), []Repository{items[pivotIndex]}...), largerItems...)
		//merged := MergeLists(smaller_items, items[pivot_index], larger_items)

		for j := 0; j < len(items); j++ {
			items[j] = merged[j]
		}
	}
}

func Uptime() time.Duration {
	return time.Since(StartTime)
}

//Writes out an Internal Server Error if the error parameter is not nil.
func CheckError(error error, w http.ResponseWriter){
	if error != nil{			//If there is an error
		//Print this along with internal server error status code.
		http.Error(w, error.Error(), http.StatusInternalServerError)
	}
}
