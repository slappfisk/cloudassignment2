package cmd

import "time"

/**
RepoID saves the ID for the repository
RepoName saves the "username/repository name" for the repository
Commits saves the number of commits in the repository
*/
type Repository struct{
	RepoID int `json:"id"`
	RepoName string `json:"path_with_namespace"`
	Commits int
}

//saves the ID for the commit in the repository
type NumCommits struct{
	Commits int `json:"id"`
}

type Language struct{
	Key string
	Value int
}

type Status struct {
	GitLabStatus	int 	// indicates whether gitlab service is available based on HTTP status code
	UpTime			int		// seconds since service start
	Version 		string	//Status
}

type Webhooks struct{
	Id			int
	Event		string			`json:"event"`
	Url			string			`json:"url"`
	TimeStamp	time.Time
}

type Invocation struct{
	Event	string	`json:"eventtype"`		// event type as per specification for registration
	Params	int							// parameters passed along in triggering request
	TimeStamp time.Time						// timestamp of request
}